package infotecs.services;

import infotecs.dal.domain.entities.PointEntity;
import infotecs.dal.domain.repositories.PointEntityRepository;
import infotecs.dtos.PointDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PointsService {

    @Autowired
    private PointEntityRepository pointEntityRepository;

    public List<PointEntity> getPoints() {
        return pointEntityRepository.findAll();
    }

    public void addPoint(PointDTO pointDTO) {
        PointEntity pointEntity = new PointEntity()
                .setAddress(pointDTO.getAddress())
                .setName(pointDTO.getName());

        pointEntityRepository.save(pointEntity);
    }

    public PointEntity getPoint(long id) {
        return pointEntityRepository.findOne(id);
    }
}
