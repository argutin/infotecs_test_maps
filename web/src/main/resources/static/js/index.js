ymaps.ready(init);
var myMap;
function init() {
    myMap = new ymaps.Map("yMap", {
        center: [55.76, 37.64],
        zoom: 7
    });
    var list = Array.prototype.slice.call(document.getElementsByName("addressHidden"));
    list.forEach(function (address) {
        ymaps.geoQuery(ymaps.geocode(address.value, {
            results: 1
        }).then(function (res) {
            // Выбираем первый результат геокодирования.
            var firstGeoObject = res.geoObjects.get(0);

            myMap.geoObjects.add(firstGeoObject);
        }));
    });
}