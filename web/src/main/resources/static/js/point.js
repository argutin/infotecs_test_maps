ymaps.ready(init);
var myMap;
var address = document.getElementById("address").value;

function init() {
    myMap = new ymaps.Map("yMap", {
        center: [55.76, 37.64],
        zoom: 7
    });

    ymaps.geocode(address, {
        results: 1
    }).then(function (res) {

        var firstGeoObject = res.geoObjects.get(0),

        bounds = firstGeoObject.properties.get('boundedBy');

        myMap.geoObjects.add(firstGeoObject);

        myMap.setBounds(bounds, {
            checkZoomRange: true
        });
    });
}