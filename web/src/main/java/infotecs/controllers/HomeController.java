package infotecs.controllers;

import infotecs.dtos.PointDTO;
import infotecs.services.PointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    private PointsService pointsService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("index");
        model.addObject("allPoints", pointsService.getPoints());

        return model;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String indexSubmit(@ModelAttribute PointDTO point) {
        pointsService.addPoint(point);

        return "redirect:/";
    }
}