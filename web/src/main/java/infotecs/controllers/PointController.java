package infotecs.controllers;

import infotecs.services.PointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PointController {

    @Autowired
    private PointsService pointsService;

    @RequestMapping(value = "/point", method = RequestMethod.GET)
    public String index(@RequestParam("pointId") Long pointId, ModelMap model) {
        model.addAttribute("point", pointsService.getPoint(pointId));

        return "point";
    }
}
