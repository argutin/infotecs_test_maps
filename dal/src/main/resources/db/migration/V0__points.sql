CREATE SEQUENCE points_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

ALTER TABLE points_sequence
  OWNER TO infotecs_test_user;

CREATE TABLE points
(
  id bigint DEFAULT nextval('points_sequence'::regclass) NOT NULL PRIMARY KEY,
    name character varying(1024) NOT NULL,
    address character varying(1024) NOT NULL,
    version bigint
);

ALTER TABLE points
  OWNER TO infotecs_test_user;