package infotecs.dal.domain.repositories;

import infotecs.dal.domain.entities.PointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointEntityRepository extends JpaRepository<PointEntity, Long> {
}
