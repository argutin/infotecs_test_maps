package infotecs.dal.domain.entities;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "points")
@SequenceGenerator(sequenceName = "points_sequence", name = "default_generator", allocationSize = 1)
public class PointEntity extends BaseEntity {
    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public PointEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public PointEntity setAddress(String address) {
        this.address = address;
        return this;
    }
}
