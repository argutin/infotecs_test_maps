package infotecs.dal.domain.entities;

import javax.persistence.*;

@MappedSuperclass
class BaseEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_generator")
    private Long id;

    @Version
    private Long version;

    public Long getId() {
        return id;
    }

    public BaseEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public BaseEntity setVersion(Long version) {
        this.version = version;
        return this;
    }
}
